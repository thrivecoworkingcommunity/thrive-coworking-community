THRIVE Coworking Community

A shared coworking space in Lindsay, Kawartha Lakes where successful businesses + entrepreneurs go to THRIVE. Be part of an energized and engaged community of business owners and entrepreneurs that understand what you are going through, what you are trying to achieve, and that you want to THRIVE.

Address: 18 Kent Street West, Lindsay, ON K9V 2Y1, Canada

Phone: 705-995-2034

Website: https://www.thriveonkent.com
